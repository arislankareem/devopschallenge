#! /bin/bash
sudo yum update -y
sudo yum install docker -y
sudo usermod -a -G docker ec2-user
newgrp docker
sudo systemctl enable docker.service
sudo systemctl start docker.service
# sudo yum install -y httpd
# sudo systemctl  start httpd
# sudo systemctl  enable httpd
# echo "<h1>Deployed via Terraform</h1>" | sudo tee /var/www/html/index.html