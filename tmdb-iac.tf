terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.49.0"
    }
  }
}

provider "aws" {
  region = "ap-southeast-1"
  access_key = var.AWS_ACCESS_KEY_ID
  secret_key = var.AWS_SECRET_ACCESS_KEY

}

resource "aws_key_pair" "key_pair" {
    public_key = tls_private_key.rsa.public_key_openssh
    key_name = "tf-key"
}

resource "tls_private_key" "rsa" {
  algorithm = "RSA"
  rsa_bits = 4096
}

resource "local_file" "key_pair" {
  filename = "tf-key.pem"  
  content = tls_private_key.rsa.private_key_pem
}
resource "aws_security_group" "tmdb" {
  name = "tmdb"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = "22"
    to_port   = "22"
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = "80"
    to_port   = "80"
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "web" {
  ami = "ami-0a95d2cc973f39afc"
  instance_type = "t3.micro"
  associate_public_ip_address = true
  vpc_security_group_ids = [aws_security_group.tmdb.id]
  key_name = aws_key_pair.key_pair.key_name
  user_data = "${file("apache_config.sh")}"
    

  tags = {
    Name = "tmdb"
  }
}


resource "local_file" "instace_ip" {
  content = aws_instance.web.public_ip
  filename = "ip.txt"
}

output "ip" {
  value = aws_instance.web.public_ip
}