## To run from CMD

In project folder run

npm start

## Build Docker

In project folder run

docker build -t {image name} . 

docker run -p 3000 {image name}  

# Challenges Attempted

1. Implement CI pipeline on GitLab
2. IAC deployment of the application on AWS cloud. Infrastructure as code used Terraform, CI/CD used GitLab.

# Description

## The pipeline will  have the following stages: 
  - build
  - lint
  - test
  - docker-build
  - terraform
  - deploy

- build
- lint
- test
Pipeline will build, lint and test the code in the repo. Currently only used high level code for the three stages.

- docker-build

Once done it will build a docker image and push the docker image to docker hub. You can find the image on the URL https://hub.docker.com/repository/docker/arislankareem/devopschallenge/general.

- terraform

Once the docker-build stage is completed; pipeline will trigger the terraform code to create an Ec2 instance with port 80 and 22 open to public on a default VPC(Open to public is not entertained, only for easyness of demo). Along with it will create a private_key. During the instance creation we will install the docker using the userdata.  Once this is done an artifact of the instance public ip and private_key is generated which is going to be used on the next stage deploy. 

- deploy

On this stage, it will fetch the public ip and key and make the key permission proper. Using the SSH command the stage will login to the instance and pull and run the  application image on the server. 

# Pipeline 

![alt text](image-1.png)

Once the last stage is completed we will be able to see the application on the instance public ip.

![alt text](image.png)
  

# Time Spent

- .30 hour for the 2 challenges combined solution
- 5 hours for the initial draft implementation
- 8 hours trouble shooting
    - faced issue while using terraform image
    - Dockerfile command corrected
    - Upgraded Dockerfile
    - docker dind version used was showing issue with the image build. 


# Extra Credi Added

Terraform to build vpc,sg, ec2, target group and Application load balancer