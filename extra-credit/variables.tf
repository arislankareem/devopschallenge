### General ###
variable "project" {
  type = string
  default = ""
}
variable "env" {
  type = string
  default = ""
}
variable "tags" {
  type = map(string)
  default = {
    "name" = "value"
  }
}
### General ###

### VPC ###
variable "vpc_cidr" {
  type = string
  default = ""
}
variable "public1_cidr" {
  type = string
  default = ""
}
variable "public2_cidr" {
  type = string
  default = ""
}
variable "private1_cidr" {
  type = string
  default = ""
}
variable "public1_subnet_id" {
  type = string
  default = ""
}
variable "public2_subnet_id" {
  type = string
  default = ""
}
variable "private1_subnet_id" {
  type = string
  default = ""
}

### VPC ###
### sg ###
variable "vpc_id" {
  type = string
  default = ""
}
variable "alb_sg_id" {
  type = list(string)
  default = [ "" ]
}
### SG ###

### LB ### 
variable "instance_id" {
  type = string
  default = ""
}
variable "webapp_sg_id" {
  type = list(string)
  default = [ "" ]
}

### Creds ###
variable "AWS_ACCESS_KEY_ID" {
}
variable "AWS_SECRET_ACCESS_KEY" {
  
}