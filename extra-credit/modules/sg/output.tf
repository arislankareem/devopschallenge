output "alb_sg_id" {
  value = aws_security_group.ALB.id
}
output "webapp_sg_id" {
  value = aws_security_group.webapp.id
}