resource "aws_security_group" "ALB" {
  name = "${var.project}-ALB-SG"
  description = "Public internet access"
  vpc_id = "${var.vpc_id}"
  tags = merge(var.tags, {
           Name        = "${var.project}-${var.env}-ALB-SG"
    })

}
 
resource "aws_vpc_security_group_egress_rule" "ALBOutbound" {
  ip_protocol    = "-1"
  cidr_ipv4 = "0.0.0.0/0"
  description = "Outbound connection is allowed to all"
  security_group_id = aws_security_group.ALB.id
}
 
resource "aws_vpc_security_group_ingress_rule" "http_public_in" {
  
  from_port         = 80
  to_port           = 80
  ip_protocol       = "tcp"
  cidr_ipv4 = "0.0.0.0/0"
  description = "ALB HTTP access from anyware"
  security_group_id = aws_security_group.ALB.id
}
