### General ###
variable "project" {
  type = string
  default = ""
}
variable "env" {
  type = string
  default = ""
}
variable "tags" {
  type = map(string)
  default = {
    "name" = "value"
  }
}
### General ###
### SG ###
variable "vpc_id" {
  type = string
  default = ""
}

variable "alb_sg_id" {
  type = string
  default = ""
}