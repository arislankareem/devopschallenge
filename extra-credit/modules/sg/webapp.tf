resource "aws_security_group" "webapp" {
  name = "${var.project}-App-SG"
  description = "Group assigned to Bastion/Jumpserver"
  vpc_id = "${var.vpc_id}"
  tags = merge(var.tags, {
    Name = "${var.project}-${var.env}-WebappSG"
  })
 
}

resource "aws_vpc_security_group_ingress_rule" "HTTP" {
  
  from_port         = 80
  to_port           = 80
  ip_protocol       = "tcp"
  referenced_security_group_id  =  aws_security_group.ALB.id      # to point one sg id to this rule
  description = "https access to application server from ALB Only"
  security_group_id = aws_security_group.webapp.id
}
resource "aws_vpc_security_group_egress_rule" "webappout" {
  ip_protocol    = "-1"
  cidr_ipv4 = "0.0.0.0/0"
  description = "Outbound connection is allowed to all"
  security_group_id = aws_security_group.webapp.id    # Security group to which this rule needs to applied. 
}