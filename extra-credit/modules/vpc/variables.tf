variable "vpc_cidr" {
  type = string
  default = ""
}
variable "public1_cidr" {
  type = string
  default = ""
}
variable "public2_cidr" {
  type = string
  default = ""
}
variable "private1_cidr" {
  type = string
  default = ""
}
variable "project" {
  type = string
  default = ""
}
variable "env" {
  type = string
  default = ""
}
variable "tags" {
  type = map(string)
  default = {
    "name" = "value"
  }
}