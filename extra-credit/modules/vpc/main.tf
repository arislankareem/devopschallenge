# ---------------------------------------------------
# Declare the data source
# ---------------------------------------------------
data "aws_availability_zones" "available" {
  state = "available"
}


resource "aws_internet_gateway" "igw" {
    
  vpc_id = aws_vpc.myvpc.id
  tags = merge(var.tags, {
           Name        = "${var.project}-igw"
    })
}

# ---------------------------------------------------
# Creating VPC
# ---------------------------------------------------
resource "aws_vpc" "myvpc" {
    
  cidr_block           = var.vpc_cidr
  instance_tenancy     = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = merge(var.tags, {
           Name        = "${var.project}-${var.env}"
    })

}

resource "aws_subnet" "public" {
    
  vpc_id                   = aws_vpc.myvpc.id
  cidr_block               = var.public1_cidr
  availability_zone        = data.aws_availability_zones.available.names[0]
  map_public_ip_on_launch  = true
  tags = merge(var.tags, {
    Name = "${var.project}-public1"
  })
}
resource "aws_subnet" "public2" {
    
  vpc_id                   = aws_vpc.myvpc.id
  cidr_block               = var.public2_cidr
  availability_zone        = data.aws_availability_zones.available.names[1]
  map_public_ip_on_launch  = true
  tags = merge(var.tags, {
    Name = "${var.project}-public1"
  })
}
resource "aws_subnet" "private1" {
    
  vpc_id                   = aws_vpc.myvpc.id
  cidr_block               = var.private1_cidr
  availability_zone        = data.aws_availability_zones.available.names[0] 
  map_public_ip_on_launch  = false
  tags = merge(var.tags, {
    Name = "${var.project}-private1"
  })
}