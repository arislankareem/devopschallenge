resource "aws_key_pair" "key_pair" {
    public_key = tls_private_key.rsa.public_key_openssh
    key_name = "tf-key"
}

resource "tls_private_key" "rsa" {
  algorithm = "RSA"
  rsa_bits = 4096
}

resource "local_file" "key_pair" {
  filename = "tf-key.pem"  
  content = tls_private_key.rsa.private_key_pem
}
resource "aws_instance" "web" {
  ami = "ami-0a95d2cc973f39afc"
  instance_type = "t3.micro"
  associate_public_ip_address = false
  vpc_security_group_ids = var.webapp_sg_id
  key_name = aws_key_pair.key_pair.key_name
  subnet_id = var.private1_subnet_id
  user_data = file("${path.module}/apache_config.sh")
    

  tags = {
    Name = "tmdb"
  }
}
