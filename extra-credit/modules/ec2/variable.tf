### General ###
variable "project" {
  type = string
  default = ""
}
variable "env" {
  type = string
  default = ""
}
variable "tags" {
  type = map(string)
  default = {
    "name" = "value"
  }
}
### General ###
variable "webapp_sg_id" {
  type = list(string)
  default = [ "" ]
}

variable "private1_subnet_id" {
  type = string
  default = ""
}
