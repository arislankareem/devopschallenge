### General ###
variable "project" {
  type = string
  default = ""
}
variable "env" {
  type = string
  default = ""
}
variable "tags" {
  type = map(string)
  default = {
    "name" = "value"
  }
}
### General ###
variable "alb_sg_id" {
  type = list(string)
  default = [ "" ]
}
variable "vpc_id" {
  type = string
  default = ""
}
variable "instance_id" {
  type = string
  default = ""
}
variable "public1_subnet_id" {
  type = string
  default = ""
}
variable "public2_subnet_id" {
  type = string
  default = ""
}