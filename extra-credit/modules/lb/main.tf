resource "aws_lb" "test" {
  name               = "alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = var.alb_sg_id
  subnets = ["${var.public1_subnet_id}", "${var.public2_subnet_id}"]
  tags = merge(var.tags, {
           Name        = "${var.project}-${var.env}-lb"
    })
}

resource "aws_lb_target_group" "tmdb" {
  name     = "tf-example-lb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id
  tags = merge(var.tags, {
           Name        = "${var.project}-${var.env}-lb"
    })
}
resource "aws_lb_target_group_attachment" "lb" {
  target_group_arn = aws_lb_target_group.tmdb.arn
  target_id        =  var.instance_id
  port             = 80
}
resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.test.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tmdb.arn
  }
}