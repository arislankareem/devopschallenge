module "vpc" {
  source = "./modules/vpc"
  project = var.project
  env = var.env
  vpc_cidr = var.vpc_cidr
  public1_cidr = var.public1_cidr
  public2_cidr = var.public2_cidr
  private1_cidr = var.private1_cidr
}
module "sg" {
  source = "./modules/sg"
  project = var.project
  env = var.env
  vpc_id = module.vpc.vpc_id
  
}
module "lb" {
  source = "./modules/lb"
  project = var.project
  env = var.env
  alb_sg_id = [module.sg.alb_sg_id]
  vpc_id = module.vpc.vpc_id
  instance_id = module.ec2.instance_id
  public1_subnet_id = module.vpc.public1_subnet_id
  public2_subnet_id = module.vpc.public2_subnet_id
}
module "ec2" {
  source = "./modules/ec2"
  project = var.project
  env = var.env
  webapp_sg_id = [module.sg.webapp_sg_id]
  private1_subnet_id = module.vpc.private1_subnet_id

}